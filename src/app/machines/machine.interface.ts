import { uuid } from '../shared/interfaces/uuid.interface';

export enum MachineStatus {
  ON = 'on',
  OFF = 'off'
}

// when retrieving a machine from the REST endpoint (`/machines/:machineId`)
// you'll get an object of the following type
export interface Machine {
  id: uuid;
  name?: string;
  /**
   * status could be one of the Machine entity field
   * ideally we could seperate Machine entity variations into data model, view model and
   * even response model, but here for simplicity, I'll use just one model for all purposes
   * with optional fields
   */
  status?: MachineStatus;
  /**
   * logs keep track of all the machine log updates, keep logs here attached to Machine entity to
   * avoid a seperate "MachineLogState" for simplicity
   */
  logs?: MachineStatusLog[];

  onTime?: number;
  offTime?: number;
}

// when subscribing to the websocket and the event 'MACHINE_STATUS_CHANGES'
// you'll get events of the following type
export interface MachineStatusFromWebSocket {
  id: uuid;
  name: string; // this field also included in web socket response
  status: MachineStatus;
}

/**
 * Keep track of all Machine logs
 */
export interface MachineStatusLog {
  status: MachineStatus;
  timestamp: Date;
}
