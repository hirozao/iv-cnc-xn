import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from '../../state';
import { ActivatedRoute } from '@angular/router';
import { selectMachines } from '../machines.selectors';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MachineStatusLog, Machine } from '../machine.interface';

@Component({
  selector: 'app-machine',
  templateUrl: './machine.component.html',
  styleUrls: ['./machine.component.scss']
})
export class MachineComponent implements OnInit {
  machine$: Observable<Machine>;

  constructor(private store: Store<State>, private route: ActivatedRoute) {}

  ngOnInit() {
    // snapshot is used here for simplicity,
    // we can also take the observable version of this id
    // to achieve goal of switching between different machine logs
    // without component reload
    const id = this.route.snapshot.params.id;

    this.machine$ = this.store.pipe(
      select(selectMachines),
      map(machines => machines[id])
    );
  }
}
