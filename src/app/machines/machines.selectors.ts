import { machinesAdapter, MachinesState } from './machines.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const selectMachinesState = createFeatureSelector<MachinesState>('machines');

const { selectIds, selectEntities, selectAll, selectTotal } = machinesAdapter.getSelectors();

/**
 * this selector can be used to check if a machine is new or existing
 */
export const selectMachineIds = createSelector(selectMachinesState, selectIds);

/**
 * get all machine entities
 */
export const selectMachines = createSelector(selectMachinesState, selectEntities);
