import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Socket } from 'ng-socket-io';
import {
  START_LISTEN_STATUS,
  UpdateMachineStatus,
  DISCOVER_NEW_MACHINE,
  DiscoverNewMachine,
  DiscoverNewMachineSuccess,
  DiscoverNewMachineFail
} from './machines.actions';
import { map, withLatestFrom, concatMap, mergeMap, catchError, switchMap } from 'rxjs/operators';
import { MachineStatusFromWebSocket } from './machine.interface';
import { Store } from '@ngrx/store';
import * as machineSelectors from './machines.selectors';
import { MachinesState } from './machines.reducer';
import { MachinesService } from './machines.service';
import { of } from 'rxjs';

@Injectable()
export class MachinesEffects {
  @Effect()
  machinesStatusesChanges$ = this.actions$.pipe(
    ofType(START_LISTEN_STATUS),
    // use mergeMap or concatMap to catch all status changes (exaustMap and switchMap may lost events)
    concatMap(() => this.socket.fromEvent<MachineStatusFromWebSocket>('MACHINE_STATUS_CHANGES')),
    withLatestFrom(this.store.select(machineSelectors.selectMachineIds)),
    switchMap(([statusFromWS, ids]: [MachineStatusFromWebSocket, string[]]) => {
      // discover new machine takes time and we don't want to duplicate or miss any of those requests
      // so here we add the machine record to entities list first, then fire another action to do
      // the time consuming machine info update job from remote endpoint, this way we can garanteen
      // each new machine fetch info once and only once, meanwhile not blocking status log notification
      // from web socket
      return ids.includes(statusFromWS.id)
        ? [new UpdateMachineStatus(statusFromWS)]
        : [new UpdateMachineStatus(statusFromWS), new DiscoverNewMachine(statusFromWS.id)];
    })
  );

  @Effect()
  discoverNewMachine$ = this.actions$.pipe(
    ofType(DISCOVER_NEW_MACHINE),
    mergeMap((action: DiscoverNewMachine) => this.machinesService.getMachine(action.payload)),
    map(machine => new DiscoverNewMachineSuccess(machine)),
    // demo only, real project need some error handling logic
    catchError(err => of(new DiscoverNewMachineFail()))
  );

  constructor(
    private actions$: Actions,
    private socket: Socket,
    private store: Store<MachinesState>,
    private machinesService: MachinesService
  ) {}
}
