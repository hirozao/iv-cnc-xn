import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Machine } from './machine.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MachinesService {
  constructor(private api: ApiService) {}

  getMachine(id: string): Observable<Machine> {
    return this.api.get('machines/' + id);
  }
}
