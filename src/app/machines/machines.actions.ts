import { Action } from '@ngrx/store';
import { MachineStatusLog, Machine, MachineStatusFromWebSocket } from './machine.interface';

export const START_LISTEN_STATUS = '[Machines] START_LISTEN_STATUS';
export class StartListenStatus implements Action {
  readonly type = START_LISTEN_STATUS;

  constructor() {}
}

export const STOP_LISTEN_STATUS = '[Machines] STOP_LISTEN_STATUS';
export class StopListenStatus implements Action {
  readonly type = STOP_LISTEN_STATUS;

  constructor() {}
}

export const UPDATE_MACHINE_STATUS = '[Machines] UPDATE_MACHINE_STATUS';
export class UpdateMachineStatus implements Action {
  readonly type = UPDATE_MACHINE_STATUS;

  constructor(public payload: MachineStatusFromWebSocket) {}
}

export const DISCOVER_NEW_MACHINE = '[Machines] DISCOVER_NEW_MACHINE';
export class DiscoverNewMachine implements Action {
  readonly type = DISCOVER_NEW_MACHINE;

  // payload is machine id
  constructor(public payload: string) {}
}

export const DISCOVER_NEW_MACHINE_SUCCESS = '[Machines] DISCOVER_NEW_MACHINE_SUCCESS';
export class DiscoverNewMachineSuccess implements Action {
  readonly type = DISCOVER_NEW_MACHINE_SUCCESS;

  constructor(public payload: Machine) {}
}

export const DISCOVER_NEW_MACHINE_FAIL = '[Machines] DISCOVER_NEW_MACHINE_FAIL';
export class DiscoverNewMachineFail implements Action {
  readonly type = DISCOVER_NEW_MACHINE_FAIL;
}

export type MachinesActionsUnion =
  | StartListenStatus
  | StopListenStatus
  | UpdateMachineStatus
  | DiscoverNewMachine
  | DiscoverNewMachineSuccess;
