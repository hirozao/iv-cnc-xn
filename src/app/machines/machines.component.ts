import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { State } from '../state';
import { selectMachines } from './machines.selectors';
import { Machine } from './machine.interface';

@Component({
  selector: 'app-machines',
  templateUrl: './machines.component.html',
  styleUrls: ['./machines.component.scss']
})
export class MachinesComponent implements OnInit {
  machines$ = this.store.select(selectMachines);

  constructor(private store: Store<State>) {}

  ngOnInit() {}

  trackByMachineId(entity: { key: string; value: Machine }) {
    return entity.key;
  }
}
