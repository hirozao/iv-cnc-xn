import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Machine, MachineStatusLog, MachineStatus } from './machine.interface';
import { MachinesActionsUnion, UPDATE_MACHINE_STATUS, DISCOVER_NEW_MACHINE_SUCCESS } from './machines.actions';

export type MachinesState = EntityState<Machine>;

export const machinesAdapter: EntityAdapter<Machine> = createEntityAdapter<Machine>();

export const initialState: MachinesState = machinesAdapter.getInitialState();

export function machinesReducer(state: MachinesState = initialState, action: MachinesActionsUnion): MachinesState {
  switch (action.type) {
    // handle the actions you want to here
    case UPDATE_MACHINE_STATUS: {
      const statusFromWS = action.payload;

      // get current timestamp for the status log
      const timestamp = new Date();

      // get machine entity to check if it exists already
      const machine = state.entities[statusFromWS.id];

      // the log that need to insert into machine logs
      const log = { status: statusFromWS.status, timestamp };

      let logs: MachineStatusLog[];
      let prevTimestamp: Date;

      // machine logs
      if (machine) {
        logs = [...machine.logs, log];
        prevTimestamp = machine.logs[machine.logs.length - 1].timestamp;
      } else {
        logs = [log];
        prevTimestamp = timestamp;
      }

      const newMachine: any = {
        id: statusFromWS.id,
        status: statusFromWS.status,
        logs
      };

      const timespan = +timestamp - +prevTimestamp;

      if (statusFromWS.status === MachineStatus.OFF) {
        newMachine.onTime = (machine ? machine.onTime || 0 : 0) + timespan;
      } else if (statusFromWS.status === MachineStatus.ON) {
        newMachine.offTime = (machine ? machine.offTime || 0 : 0) + timespan;
      }

      // console.log('machine', machine, newMachine, timespan);

      // upsertOne() can add or update entity
      return machinesAdapter.upsertOne(newMachine, state);
    }

    case DISCOVER_NEW_MACHINE_SUCCESS: {
      const machine = action.payload;
      return machinesAdapter.updateOne({ id: machine.id, changes: { ...machine } }, state);
    }

    default:
      return state;
  }
}
