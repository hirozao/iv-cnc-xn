import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
/**
 * project specific HTTP request base service
 */
export class ApiService {
  private baseUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {}

  get<T>(path: string, params?: { [key: string]: any }): Observable<T> {
    return this.makeHttp(path, 'GET', params);
  }

  put<T>(path: string, body?: { [key: string]: any }, params?: { [key: string]: any }): Observable<T> {
    return this.makeHttp(path, 'PUT', params, body);
  }

  post<T>(path: string, body?: { [key: string]: any }, params?: { [key: string]: any }): Observable<T> {
    return this.makeHttp(path, 'POST', params, body);
  }

  delete<T>(path: string, params?: { [key: string]: any }): Observable<T> {
    return this.makeHttp(path, 'DELETE', params);
  }

  makeHttp<T>(
    path: string,
    method: string,
    params?: { [key: string]: any },
    body?: { [key: string]: any }
  ): Observable<T> {
    return this.http.request<T>(method, this.baseUrl + path, {
      body: body,
      headers: {
        ...params,
        'Content-Type': 'application/json'
        // in real projects usually put some common headers here such as Authorization, Accept, Access-Token, Request-Token etc.
      }
    });
  }
}
