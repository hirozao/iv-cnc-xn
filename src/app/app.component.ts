import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from './state';
import { StartListenStatus } from './machines/machines.actions';
import { selectMachines } from './machines/machines.selectors';
import { map } from 'rxjs/operators';
import { MachineStatus } from './machines/machine.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  // app component template contains broken links which are not described in the original task README
  // so I've made up 2 fields counting total ON/OFF status machines just for fun
  statistics$ = this.store.pipe(
    select(selectMachines),
    map(machines => {
      let on = 0;
      let off = 0;

      for (const id of Object.keys(machines)) {
        const m = machines[id];

        on += m.onTime || 0;
        off += m.offTime || 0;
      }

      on = on / 1000;
      off = off / 1000;

      const onPercent = (on / (on + off)) * 100 || 0;
      const offPercent = (off / (on + off)) * 100 || 0;

      return { on, off, onPercent, offPercent };
    })
  );

  constructor(private store: Store<State>) {}

  ngOnInit() {
    // listening web socket status logs at app startup
    this.store.dispatch(new StartListenStatus());
  }
}
